db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "fruitOnSale"}

])

db.fruits.aggregate([
	{$match: {stock: {$gte:20}}},
	{$count: "enoughStock"}
])

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
])

// if onSale = true, based on sample output
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
])
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
])


// if based on instruction where onSale is not specified
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
])

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
])